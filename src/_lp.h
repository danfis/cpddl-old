/***
 * cpddl
 * --------
 * Copyright (c)2017 Daniel Fiser <danfis@danfis.cz>
 *
 *  This file is part of cpddl.
 *
 *  Distributed under the OSI-approved BSD License (the "License");
 *  see accompanying file BDS-LICENSE for details or see
 *  <http://www.opensource.org/licenses/bsd-license.php>.
 *
 *  This software is distributed WITHOUT ANY WARRANTY; without even the
 *  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the License for more information.
 */

#ifndef __PDDL__LP_H__
#define __PDDL__LP_H__

#include "pddl/config.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

# define PDDL_LP_GET_NUM_THREADS(flags) (((flags) >> 8u) & 0x3fu)
# define PDDL_LP_NUM_THREADS_AUTO 0x3fu

struct _pddl_lp_cls_t {
    int solver_id;
    const char *solver_name;
    pddl_lp_t *(*new)(int rows, int cols, unsigned flags, pddl_err_t *err);
    void (*del)(pddl_lp_t *);
    void (*set_obj)(pddl_lp_t *lp, int i, double coef);
    void (*set_var_range)(pddl_lp_t *lp, int i, double lb, double ub);
    void (*set_var_free)(pddl_lp_t *lp, int i);
    void (*set_var_int)(pddl_lp_t *lp, int i);
    void (*set_var_binary)(pddl_lp_t *lp, int i);
    void (*set_coef)(pddl_lp_t *lp, int row, int col, double coef);
    void (*set_rhs)(pddl_lp_t *lp, int row, double rhs, char sense);
    void (*add_rows)(pddl_lp_t *lp, int cnt, const double *rhs, const char *sense);
    void (*del_rows)(pddl_lp_t *lp, int begin, int end);
    int (*num_rows)(const pddl_lp_t *lp);
    void (*add_cols)(pddl_lp_t *lp, int cnt);
    void (*del_cols)(pddl_lp_t *lp, int begin, int end);
    int (*num_cols)(const pddl_lp_t *lp);
    int (*solve)(pddl_lp_t *lp, double *val, double *obj);
    void (*write)(pddl_lp_t *lp, const char *fn);
    void (*tune)(pddl_lp_t *lp, unsigned flag);
};
typedef struct _pddl_lp_cls_t pddl_lp_cls_t;

struct _pddl_lp_t {
    pddl_lp_cls_t *cls;
    pddl_err_t *err;
};

extern pddl_lp_cls_t pddl_lp_not_available;
extern pddl_lp_cls_t pddl_lp_cplex;
extern pddl_lp_cls_t pddl_lp_gurobi;
extern pddl_lp_cls_t pddl_lp_lpsolve;
extern pddl_lp_cls_t pddl_lp_glpk;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __PDDL__LP_H__ */
