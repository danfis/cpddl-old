-include Makefile.local
-include Makefile.include

CFLAGS += -I.
CFLAGS += -Wno-sizeof-pointer-div
CFLAGS += $(BLISS_CFLAGS)
CFLAGS += $(CLIQUER_CFLAGS)
CFLAGS += $(CUDD_CFLAGS)
CFLAGS += $(LP_CFLAGS)

CPPFLAGS += -Wno-ignored-attributes
CPPFLAGS += -I.
CPPFLAGS += $(CPOPTIMIZER_CPPFLAGS)

CPPCHECK_FLAGS += --platform=unix64 --enable=all -I.

TARGETS  = libpddl.a

OBJS  = alloc
OBJS += err
OBJS += hfunc
OBJS += google-city-hash
OBJS += rand-mt
OBJS += sort
OBJS += qsort
OBJS += segmarr
OBJS += extarr
OBJS += pairheap
OBJS += hashset
OBJS += rbtree
OBJS += htable
OBJS += fifo
OBJS += lp
OBJS += lp-cplex
OBJS += lp-lpsolve
OBJS += lp-gurobi
OBJS += lp-glpk
OBJS += lisp
OBJS += require
OBJS += type
OBJS += param
OBJS += obj
OBJS += pred
OBJS += fact
OBJS += action
OBJS += prep_action
OBJS += pddl
OBJS += unify
OBJS += compile_in_lifted_mgroup
OBJS += cond
OBJS += cond_arr
OBJS += strips
OBJS += strips_op
OBJS += strips_fact_cross_ref
OBJS += strips_maker
OBJS += sqlite3
OBJS += sql_grounder
OBJS += strips_ground_tree
OBJS += strips_ground
OBJS += strips_ground_sql
OBJS += strips_ground_datalog
OBJS += action_args
OBJS += ground_atom
OBJS += profile
OBJS += lifted_mgroup
OBJS += lifted_mgroup_infer
OBJS += lifted_mgroup_htable
OBJS += mgroup
OBJS += mgroup_projection
OBJS += mutex_pair
OBJS += pddl_file
OBJS += plan_file
OBJS += irrelevance
OBJS += h1
OBJS += h2
OBJS += h3
OBJS += hm
OBJS += disambiguation
OBJS += bitset
OBJS += set
OBJS += fdr_var
OBJS += fdr_part_state
OBJS += fdr_op
OBJS += fdr
OBJS += fdr_state_packer
OBJS += fdr_state_pool
OBJS += fdr_state_space
OBJS += strips_state_space
OBJS += sym
OBJS += famgroup
OBJS += pot
OBJS += lm_cut
OBJS += hpot
OBJS += hflow
OBJS += hmax
OBJS += hadd
OBJS += hff
OBJS += pq
OBJS += mg_strips
OBJS += preprocess
OBJS += cg
OBJS += graph
OBJS += clique
OBJS += biclique
OBJS += fdr_app_op
OBJS += random_walk
OBJS += open_list
OBJS += open_list_splaytree1
OBJS += open_list_splaytree2
OBJS += search
OBJS += search_astar
OBJS += search_lazy
OBJS += search_lifted
OBJS += plan
OBJS += relaxed_plan
OBJS += heur
OBJS += heur_blind
OBJS += heur_lm_cut
OBJS += heur_hmax
OBJS += heur_hadd
OBJS += heur_hff
OBJS += heur_pot_state
OBJS += heur_flow
OBJS += dtg
OBJS += scc
OBJS += ts
OBJS += op_mutex_pair
OBJS += op_mutex_infer
OBJS += op_mutex_infer_ts
OBJS += op_mutex_sym_redundant
OBJS += reversibility
OBJS += invertibility
OBJS += cascading_table
OBJS += transition
OBJS += label
OBJS += labeled_transition
OBJS += trans_system
OBJS += trans_system_abstr_map
OBJS += trans_system_graph
OBJS += bdd
OBJS += bdds
OBJS += symbolic_vars
OBJS += symbolic_constr
OBJS += symbolic_trans
OBJS += symbolic_state
OBJS += symbolic_task
OBJS += symbolic_split_goal
OBJS += cost
OBJS += black_mgroup
OBJS += red_black_fdr
OBJS += outbox
OBJS += datalog
OBJS += homomorphism
OBJS += homomorphism_heur
OBJS += prune_strips
OBJS += objset
OBJS += iset
OBJS += lset
OBJS += cset
OBJS += iarr

OBJS_CPP = endomorphism

OBJS := $(foreach obj,$(OBJS),.objs/$(obj).o) $(foreach obj,$(OBJS_CPP),.objs/$(obj).cpp.o)

GEN  = pddl/objset.h
GEN += src/objset.c
GEN += pddl/iset.h
GEN += src/iset.c
GEN += pddl/lset.h
GEN += src/lset.c
GEN += pddl/cset.h
GEN += src/cset.c
GEN += pddl/iarr.h
GEN += src/iarr.c

all: $(TARGETS)

bin:
	$(MAKE) -C bin

libpddl.a: $(OBJS) Makefile
	echo "const char *pddl_version = \"$(shell git rev-parse HEAD)\";" >_version.c
	$(CC) -c -o .objs/_version.o _version.c
	rm -f _version.c
	ar cr $@ $(OBJS) .objs/_version.o
	ranlib $@

pddl/config.h:
	echo "#ifndef __PDDL_CONFIG_H__" >$@
	echo "#define __PDDL_CONFIG_H__" >>$@
	echo "" >>$@
	if [ "$(DEBUG)" = "yes" ]; then echo "#define PDDL_DEBUG" >>$@; fi
	if [ "$(USE_CLIQUER)" = "yes" ]; then echo "#define PDDL_CLIQUER" >>$@; fi
	if [ "$(USE_CUDD)" = "yes" ]; then echo "#define PDDL_CUDD" >>$@; fi
	if [ "$(USE_BLISS)" = "yes" ]; then echo "#define PDDL_BLISS" >>$@; fi
	if [ "$(USE_CPLEX)" = "yes" ]; then echo "#define PDDL_CPLEX" >>$@; fi
	if [ "$(USE_CPOPTIMIZER)" = "yes" ]; then echo "#define PDDL_CPOPTIMIZER" >>$@; fi
	if [ "$(USE_GUROBI)" = "yes" ]; then echo "#define PDDL_GUROBI" >>$@; fi
	if [ "$(USE_GLPK)" = "yes" ]; then echo "#define PDDL_GLPK" >>$@; fi
	if [ "$(USE_LPSOLVE)" = "yes" ]; then echo "#define PDDL_LPSOLVE" >>$@; fi
	if [ "$(USE_CPLEX)" = "yes" ] || [ "$(USE_GUROBI)" = "yes" ] || [ "$(USE_LPSOLVE)" = "yes" ]; then echo "#define PDDL_LP" >>$@; fi
	echo "" >>$@
	echo "#endif /* __PDDL_CONFIG_H__ */" >>$@

pddl/objset.h: src/_set_arr.h scripts/fmt_set.sh
	$(BASH) scripts/fmt_set.sh set Set pddl_obj_id_t obj Obj OBJ <$< >$@
src/objset.c: src/_set_arr.c scripts/fmt_set.sh pddl/objset.h
	$(BASH) scripts/fmt_set.sh set Set pddl_obj_id_t obj Obj OBJ <$< >$@
pddl/iset.h: src/_set_arr.h scripts/fmt_set.sh
	$(BASH) scripts/fmt_set.sh set Set int i I I <$< >$@
src/iset.c: src/_set_arr.c scripts/fmt_set.sh pddl/objset.h
	$(BASH) scripts/fmt_set.sh set Set int i I I <$< >$@
pddl/lset.h: src/_set_arr.h scripts/fmt_set.sh
	$(BASH) scripts/fmt_set.sh set Set long l L L <$< >$@
src/lset.c: src/_set_arr.c scripts/fmt_set.sh pddl/objset.h
	$(BASH) scripts/fmt_set.sh set Set long l L L <$< >$@
pddl/cset.h: src/_set_arr.h scripts/fmt_set.sh
	$(BASH) scripts/fmt_set.sh set Set long c C C <$< >$@
src/cset.c: src/_set_arr.c scripts/fmt_set.sh pddl/objset.h
	$(BASH) scripts/fmt_set.sh set Set long c C C <$< >$@
pddl/iarr.h: src/_arr.h scripts/fmt_set.sh
	$(BASH) scripts/fmt_set.sh arr Arr int i I I <$< >$@
src/iarr.c: src/_arr.c scripts/fmt_set.sh
	$(BASH) scripts/fmt_set.sh arr Arr int i I I <$< >$@

.objs/sqlite3.o: src/sqlite3.c Makefile Makefile.include
	$(CC) $(SQLITE_CFLAGS) -c -o $@ $<
.objs/%.o: src/%.c pddl/%.h pddl/config.h $(GEN)
	$(CC) $(CFLAGS) -c -o $@ $<
.objs/%.o: src/%.c pddl/config.h $(GEN)
	$(CC) $(CFLAGS) -c -o $@ $<
.objs/%.cpp.o: src/%.cpp pddl/%.h pddl/config.h $(GEN)
	$(CXX) $(CPPFLAGS) -c -o $@ $<
.objs/%.cpp.o: src/%.cpp pddl/config.h $(GEN)
	$(CXX) $(CPPFLAGS) -c -o $@ $<

%.h: pddl/config.h
%.c: pddl/config.h


clean:
	rm -f $(OBJS)
	rm -f .objs/*.o
	rm -f $(TARGETS)
	rm -f pddl/config.h
	rm -f src/*.pb.{cc,h}
	rm -f $(GEN)
	if [ -d bin ]; then $(MAKE) -C bin clean; fi;
	if [ -d test ]; then $(MAKE) -C test clean; fi;
	if [ -d doc ]; then $(MAKE) -C doc clean; fi;

mrproper: clean third-party-clean

check check-all check-valgrind check-all-valgrind check-segfault check-all-segfault check-gdb check-all-gdb:
	if [ -f t/Makefile ]; then $(MAKE) -C t $@; fi
static-check:
	$(CPPCHECK) $(CPPCHECK_FLAGS) pddl/ src/

doc:
	$(MAKE) -C doc

analyze: clean
	$(SCAN_BUILD) $(MAKE)

list-global-symbols: libpddl.a
	readelf -s libpddl.a \
        | grep GLOBAL \
        | awk '{print $$8}' \
        | sort \
        | uniq \
        | grep -v '^pddl' \
        | grep -v '^_Z.*Ilo' \
        | grep -v '^_Z.*Ilo' \
        | less

third-party: bliss cudd
third-party-clean: bliss-clean cudd-clean

bliss: third-party/bliss/libbliss.a
bliss-clean:
	rm -rf third-party/bliss
third-party/bliss/libbliss.a:
	rm -rf third-party/bliss
	cd third-party && unzip bliss-$(BLISS_VERSION).zip
	mv third-party/bliss-$(BLISS_VERSION) third-party/bliss
	cd third-party/bliss && patch -p1 <../bliss-$(BLISS_VERSION)-capi.patch
	$(MAKE) CC=$(CXX) -C third-party/bliss -f Makefile-manual lib_static
	cp third-party/bliss/src/bliss_C.h third-party/bliss/
	mv third-party/bliss/libbliss_static.a $@

lpsolve: third-party/lpsolve/liblpsolve.a
lpsolve-clean:
	$(MAKE) -C third-party/lpsolve clean
third-party/lpsolve/liblpsolve.a:
	$(MAKE) -C third-party/lpsolve

cudd: third-party/cudd/libcudd.a
cudd-clean:
	git clean -fdx third-party/cudd
	rm -f third-party/cudd/lib*.a
	rm -f third-party/cudd/cudd.h
third-party/cudd/libcudd.a:
	cd third-party/cudd && aclocal
	cd third-party/cudd && autoconf
	cd third-party/cudd && automake
	cd third-party/cudd && ./configure --disable-shared CC=$(CC) CXX=$(CXX)
	$(MAKE) -C third-party/cudd
	cp third-party/cudd/cudd/.libs/libcudd.a $@
	cp third-party/cudd/cudd/cudd.h third-party/cudd/cudd.h

sqlite-amalgam:
	unzip $(SQLITE_SRC_ZIP)
	mv sqlite-src-*/ sqlite
	cd sqlite/ && ./configure --disable-json --disable-load-extension
	cd sqlite/ && make OPTS="$(SQLITE_GEN_CFLAGS)" sqlite3.c
	cat sqlite/sqlite3.c | sed 's/sqlite3/pddl_sqlite3/g' >src/sqlite3.c
	cat sqlite/sqlite3.h | sed 's/sqlite3/pddl_sqlite3/g' >src/sqlite3.h
	rm -rf sqlite/

.PHONY: all bin clean help doc install analyze \
  examples mrproper \
  check check-all \
  check-valgrind check-all-valgrind \
  check-segfault check-all-segfault \
  check-gdb check-all-gdb \
  third-party third-party-clean \
  bliss bliss-clean \
  lpsolve lpsolve-clean \
  sqlite-amalgam
